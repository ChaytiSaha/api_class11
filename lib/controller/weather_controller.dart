import 'dart:convert';

import 'package:api_class11/constants/api/constant.dart';
import 'package:api_class11/model/weather.dart';

class WeatherController {
  Future<WeatherAPi> getWeatherByCityName(String name) async {
    var response = await networkCall.loadWeatherByCityName(name);
    return WeatherAPi.fromJson(jsonDecode(response));
  }

  Future<WeatherAPi> getWeatherByLatLong(double lat, double lon) async {
    var response = await networkCall.loadWeatherByLatLon(lat,lon);
    return WeatherAPi.fromJson(jsonDecode(response));
  }
}
